# autonomix-documentation-externe-etablissement


Ce dossier contient les programmes permettant de produire la documentation du modèle Autonomix volet établissement, dans sa version "OpenATNMX 1.0" diffusée en juillet 2022. 

La documentation est fournie sous forme de page web, accessible par [ce lien](https://drees_code.gitlab.io/public/modeles/autonomix/documentation-atnmxetablissement/).

Comme précisé dans le CONTRIBUTING, les réutilisateurs sont invités à signaler à la DREES les éventuels problèmes ou éléments manquants dans cette documentation, en écrivant à DREES_CODE@sante.gouv.fr. Il est aussi possible d'ouvrir une "issue" dans ce projet, ou de suggérer des ajouts à la documentation via une branche dédiée et une "merge request". Voir le fichier CONTRIBUTING pour plus de détails.

La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères. https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 4.0.5, le 19/07/2021.
